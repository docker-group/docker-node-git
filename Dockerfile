FROM node:14

MAINTAINER Zoltan Sandor <zoltan@sandor.xyz>

ARG CI_COMMIT_REF_NAME

RUN echo "===> Installing supporting tools..."  && \
    apt-get update -y  &&  apt-get install --fix-missing --no-install-recommends         && \
    DEBIAN_FRONTEND=noninteractive         \
    apt-get install -y                     \
        build-essential make libssl-dev libghc-zlib-dev libcurl4-gnutls-dev libexpat1-dev gettext unzip ** \
    echo "===> Installing git..."   && \
    cd /tmp/ && \
    wget https://github.com/git/git/archive/v2.21.0.zip -O latestgit.zip && \
    rm -rf git-2.21.0 && \
    unzip latestgit.zip && \
    cd git-2.21.0/ && \
    make prefix=/usr/local all && \
    make prefix=/usr/local install && \
    git --version && \
    rm -rf /var/lib/apt/lists/*

